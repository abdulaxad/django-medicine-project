from django.shortcuts import render
from medicine_app.models import Medicine

def index(request):
    medicines_list =  Medicine.objects.order_by('name')
    date_dict = {'access_record': medicines_list}
    return render(request,'medicine_app/index.html', context=date_dict)


def detail(request, pk):
    medicine_detail = Medicine.objects.get(pk=pk)
    data = {'details':medicine_detail}
    return render(request, 'medicine_app/detail.html', context=data)
