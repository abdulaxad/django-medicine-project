from django.contrib import admin
from .models import Medicine, User, Card, Cards_medicine, User_address, Order

admin.site.register(Medicine)
admin.site.register(User)
admin.site.register(Card)
admin.site.register(Cards_medicine)
admin.site.register(User_address)
admin.site.register(Order)

