from django.urls import path
from . import views

app_name = "medicine_app"

urlpatterns=[
    path('<int:pk>',views.detail, name="detail")
]