from django.db import models



class Medicine(models.Model):
    name = models.CharField(max_length=256)
    price = models.DecimalField(max_digits=8, decimal_places=2)
<<<<<<< HEAD
# <<<<<<< HEAD
    photo = models.ImageField(blank=True, null=True)
# =======
# >>>>>>> 1808c2695b16b1404092ab9912ef90b00c86e232
=======
    photo = models.ImageField(blank=True, null=True)
>>>>>>> 327daf35b2d0513c826944c9ff5bbc4dd0464486
    quantity = models.IntegerField()
    available = models.BooleanField(default=False)
    description = models.TextField()

    def __str__(self):
        return "Name: {}   Price: {}   Quantity: {}   Avialable: {} ".format(self.name,self.price,self.quantity,self.available)

class User(models.Model):
    username = models.CharField(max_length=256)
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    phone_number = models.IntegerField()
    email = models.EmailField(max_length=256)

class Card(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)

class Cards_medicine(models.Model):
    cards_id = models.ForeignKey(Card, on_delete=models.CASCADE)
    medicine_id = models.ForeignKey(Medicine, on_delete=models.CASCADE)
    count = models.IntegerField()

class User_address(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    city = models.CharField(max_length=256)
    zip_code = models.IntegerField()
    address1 = models.CharField(max_length=256)
    address2 = models.CharField(max_length=256)
    is_default = models.BooleanField()

class Order(models.Model):
    cards_id = models.ForeignKey(Card, on_delete=models.CASCADE)
    status_choices = (
    ('new','New Order'),
    ('accepted', 'Accepted'),
    ('in_progress','In progress'),
    ('comleted','Completed'),
    ('cancelled','Cancelled'),
    )
    status = models.CharField(max_length=20, choices=status_choices, default='new')
    payment_status_choices = (
        ('unpaid','Unpaid'),
        ('failed','Failed'),
        ('expired','Expired'),
        ('paid', 'Paid'),
        ('refunded', 'Refunded'),
    )
    payment_status = models.CharField(max_length=20, choices=payment_status_choices, default='unpaid')
    total_price = models.IntegerField()
    user_address_id = models.ForeignKey(User_address, on_delete=models.CASCADE)
    payment_method_choices = (
        ('cash', 'Cash'),
        ('payme', 'Payme')
    )
    payment_method = models.CharField(max_length=20, choices=payment_method_choices, default='cash')
